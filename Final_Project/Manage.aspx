﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Manage.aspx.cs" Inherits="Final_Project.Manage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
 
   <h1>Manage Pages</h1>
    <a href="addPage.aspx" id="AddPageButton">Add Page</a>

    <asp:SqlDataSource runat="server"
    id="pages_select"     
    ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
   
    <asp:DataGrid id="pages_list"
        runat="server" >
    </asp:DataGrid>

    <div id="addPageList" runat="server"></div>
</asp:Content>