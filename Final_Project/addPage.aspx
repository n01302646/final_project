﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="addPage.aspx.cs" Inherits="Final_Project.addPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Add Page</h2>
    <div id="addpageform"> 
    <h3>Input data for new page</h3>
        <asp:SqlDataSource runat="server"
        id="insertnewpage"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
 <div>
    <ASP:Label runat="server" Text="Page Title"></ASP:Label>
     <ASP:TextBox runat="server" id="pageTitle_Input"></ASP:TextBox>
     <asp:RequiredFieldValidator runat="server" controlToValidate="pageTitle_Input"  CssClass="validator" class="validator" ErrorMessage="<br/>Page Title can't be left Empty"></asp:RequiredFieldValidator>
 </div>

    <div>
    <ASP:Label runat="server" Text="Page Content"></ASP:Label>
    <ASP:TextBox runat="server" id="pageContent_Input"></ASP:TextBox>
     <asp:RequiredFieldValidator runat="server" controlToValidate="pageContent_Input" CssClass="validator" ErrorMessage="<br/>Page Content can't be left Empty"></asp:RequiredFieldValidator>   
    </div>

     <div>
    <ASP:Label runat="server" Text="Author Name"></ASP:Label>
    <ASP:TextBox runat="server" id="authorName_Input"></ASP:TextBox>
    </div>

    <ASP:Button runat="server" id="addPageButton" style="margin-top:10px;width:120px" OnClick="addnewpage" Text="Add" />
  </div>
</asp:Content>
