﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class _Default : Page
    {
        private string select_query_page_titles = "Select pageId,pageTitle,pageContent, Author_name, date_published from pages";
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = select_query_page_titles;
            pages_list.DataSource = Pages_Manual_Bind(pages_select);
            pages_list.DataBind();
        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["pageTitle"] = row["pageTitle"];
            }
            mytbl.Columns.Remove("pageId");
            myview = mytbl.DefaultView;
            return myview;
        }
    }
}