﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class addPage : System.Web.UI.Page
    {
        private string base_query = "Insert into pages(pageTitle,pageContent,author_name) Values ";
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void addnewpage(object sender, EventArgs e)
        {
            string pagetitle = pageTitle_Input.Text;
            string pageContent = pageContent_Input.Text;
            string authorName = authorName_Input.Text;

            base_query += "('" + pagetitle + "','" + pageContent + "','" + authorName +"' ); ";

            insertnewpage.InsertCommand = base_query;
            insertnewpage.Insert();
            Response.Redirect("Manage.aspx");
        }
    }
}