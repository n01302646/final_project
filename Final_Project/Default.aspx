﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Final_Project._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron" id="DefaultPageBanner">
        <h2>JOB'S CMS</h2>
        <p class="lead">Welcome to JOBS's CMS.</p>
        <p>To Add, Edit or Delete, Click pages on MANAGE in menubar</p>
    </div>

      <asp:SqlDataSource runat="server"
    id="pages_select"     
    ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
   
    <asp:DataGrid id="pages_list"
        runat="server" >
    </asp:DataGrid>

    <div id="addPageList" runat="server"></div>
</asp:Content>

