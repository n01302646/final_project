﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Final_Project
{
    public partial class viewPage : System.Web.UI.Page
    {

        public string pageId
        {
            get { return Request.QueryString["pageId"]; }
        }
        private string view_content_query =
          "SELECT * from pages";
        protected void Page_Load(object sender, EventArgs e)
        {
            view_content_query += " where pageID =" + pageId;
            page_content.SelectCommand = view_content_query;

            DataView pageview = (DataView)page_content.Select(DataSourceSelectArguments.Empty);
            DataRowView studentrowview = pageview[0];
            string title = pageview[0]["PageTitle"].ToString();
            page_title_label.InnerHtml = title;
            string content = pageview[0]["PageContent"].ToString();
            page_view_Content.InnerHtml = content;
        }

        protected void DeletePage(object sender, EventArgs e)
        {           
            string Delete_query = "DELETE FROM pages Where pageId =" + pageId;
            deletePage.DeleteCommand = Delete_query;
            deletePage.Delete();
            
        }
    }
}