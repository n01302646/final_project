﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Final_Project.Edit" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Edit Page</h2>
    <div id="editPageForm"> 
     <h3>Edit data for page</h3>
    <asp:SqlDataSource runat="server"
        id="getData"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
      <asp:SqlDataSource runat="server"
        id="updateData"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
 <div>
    <ASP:Label runat="server" Text="Page Title"></ASP:Label>
     <ASP:TextBox runat="server" id="pageTitle_Input"></ASP:TextBox>
     <asp:RequiredFieldValidator runat="server" controlToValidate="pageTitle_Input" CssClass="validator" ErrorMessage="<br/>Page Title can't be left Empty"></asp:RequiredFieldValidator>
     </div>

    <div>
    <ASP:Label runat="server" Text="Page Content"></ASP:Label>
    <ASP:TextBox runat="server" id="pageContent_Input"></ASP:TextBox>
     <asp:RequiredFieldValidator runat="server" controlToValidate="pageContent_Input" CssClass="validator" ErrorMessage="<br/>Page Content can't be left Empty"></asp:RequiredFieldValidator>   
    </div>
    <ASP:Button runat="server" id="EditButton" style="margin-top:10px;width:120px" OnClick="insertEditedData" Text="Edit" />
  </div>
</asp:Content>