﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class Manage : System.Web.UI.Page
    {
        private string select_query_page_titles = "Select pageId,pageTitle,author_name from pages";
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = select_query_page_titles;       
            pages_list.DataSource = Pages_Manual_Bind(pages_select);
            pages_list.DataBind();
        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["pageTitle"] =
                    "<a href=viewPage.aspx?pageId=" + row["pageId"] + ">" + row["pageTitle"] + "</a>";                                                      
            }
            mytbl.Columns.Remove("pageId");
            myview = mytbl.DefaultView;
            return myview;
        }

    }
}
