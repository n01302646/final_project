﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;

namespace Final_Project.usercontrols
{
    public partial class NavMenu : System.Web.UI.UserControl
    {
        public string pageTitle = "SELECT pageId,pageTitle FROM pages";
        protected void Page_Load(object sender, EventArgs e)
        {            
            Nav_Bar.SelectCommand = pageTitle;
            string listItems = pageTitle_Bind(Nav_Bar);
            ul_addLi.InnerHtml = listItems;

        }

        private string pageTitle_Bind(SqlDataSource src)
        {
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);
            //took help from Stack Overflow
            dynamic sb = new StringBuilder();
            string manageLink = String.Format("<li><a style=Float:right;Font-weight:Bold; runat=\"server\" href= \"{0}.aspx\">{1}</a></li>", "Manage", "MANAGE");
            sb.Append(manageLink);
            foreach (DataRowView row in myview)
            {
                string menuText = row["pageTitle"].ToString();               
                string line = String.Format("<li><a runat=\"server\" href= viewPage.aspx?pageId="+ row["pageId"] +" >{0}</a></li>",menuText);
                sb.Append(line);                
            }           
            return sb.ToString();
        }
    }
}

