﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class Edit : System.Web.UI.Page
    {
        public string pageId
        {
            get { return Request.QueryString["pageId"]; }
        }
           
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = Getdata(pageId);
            pageTitle_Input.Text = pagerow["pageTitle"].ToString();
            pageContent_Input.Text = pagerow["pageContent"].ToString();
        }

        protected DataRowView Getdata(string pageId)
        {
            string getQuery = "SELECT * from pages where pageID = " + pageId;
            getData.SelectCommand = getQuery;
            DataView myView = (DataView)getData.Select(DataSourceSelectArguments.Empty);
            DataRowView myRowView = myView[0];
            return myRowView;
        }

        protected void insertEditedData(object sender, EventArgs e)
        {
            string pagetitle = pageTitle_Input.Text;
            string pageContent = pageContent_Input.Text;
            string update_query = "UPDATE pages SET pageTitle = '" + pagetitle + "' , pageContent ='" + pageContent + "' WHERE pageId =" + pageId;
            updateData.UpdateCommand = update_query;
            updateData.Update();
            Response.Redirect("Manage.aspx");
        }
    }
}
