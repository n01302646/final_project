﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="viewPage.aspx.cs" Inherits="Final_Project.viewPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
 
   <h1>Page Options</h1>
    <a id="editPageLink" href="Edit.aspx?pageId=<%Response.Write(this.pageId);%>">Edit Page</a>
    <ASP:Button runat="server" id="deleteButton" onclick="DeletePage" Text="Delete" OnClientClick =" if(!Confirm('Delete Page ?')) return false;" />  <%-- Took help from Christine's Code--%>
    <h2>Page Content</h2> 
    <div id="viewPageContent">
    <asp:SqlDataSource runat="server"
        id="deletePage"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
     <asp:SqlDataSource runat="server"
        id="page_title"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
     <h3  id="page_title_label" runat="server" ></h3>
    
    <asp:SqlDataSource runat="server"
        id="page_content"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
   
    <p id="page_view_Content" runat="server"></p>
        </div>
</asp:Content>

